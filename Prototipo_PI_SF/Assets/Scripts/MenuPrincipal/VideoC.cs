using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoC : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        StartCoroutine(StartMenu());
    }

    public void CambiarEscena()
    {
        SceneManager.LoadScene("Scenes/Level");
    }

    public IEnumerator StartMenu()
    {
        yield return new WaitForSeconds(52f);
        CambiarEscena();
    }
}
