using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoContinuar : MonoBehaviour
{
    public float timer;
    public float maxTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        StartCoroutine(StartMenu());
    }

    public void CambiarEscena()
    {
        SceneManager.LoadScene("Scenes/MenuPrincipal");
    }

    public IEnumerator StartMenu()
    {
        yield return new WaitForSeconds(47f);
        CambiarEscena();
    }
}
