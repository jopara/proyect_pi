using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControler : MonoBehaviour
{
    //Variables
    public GameObject canvasMenu;
    public GameObject canvasControls;
    public GameObject canvasTeclado;
    public GameObject canvasMando;

    private void Start()
    {
        
        
        canvasMenu.SetActive(true);
        canvasControls.SetActive(false);
        canvasTeclado.SetActive(true);
        canvasMando.SetActive(false);
    }


    //this function charges the next scene
    public void Play()
    {
        SceneManager.LoadScene("Scenes/E");
    }
    // this disable the canvasMenu and activates the canvasControls
    public void Controls()
    {
        canvasMenu.SetActive(false);
        canvasControls.SetActive(true);
        Debug.Log("controles");
    }
    // this activates the canvasMenu and disables the canvasControls
    public void CanvasExit()
    {
        canvasControls.SetActive(false);
        canvasMenu.SetActive(true);
    }

    public void Teclado()
    {
        canvasTeclado.SetActive(true);
        canvasMando.SetActive(false);
    }

    public void Mando()
    {
        canvasTeclado.SetActive(false);
        canvasMando.SetActive(true);
    }


    //this method closes the game
    public void Exit()
    {
        Application.Quit();
    }
}
