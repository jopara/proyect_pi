using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHand : MuerteIntermitente
{
    public int damage = 1;
    public GameObject Player;
    public float timer;
    public float maxTimer;


    public Animator anim;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        timer = timer + Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

       if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Knockback>().DoKnockback();
            collision.GetComponent<PlayerController>().Danyos(damage);
        }
        
    }

    public void Attack()
    {
        
        anim.SetTrigger("attack");
    }

    
}
