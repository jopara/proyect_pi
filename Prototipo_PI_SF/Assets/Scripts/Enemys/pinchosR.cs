using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinchosR : MonoBehaviour
{
    public PlayerController player;
    public float speed;
    [SerializeField] Transform[] Positions;

    int NextPosIndex;
    Transform NextPos;

    // Start is called before the first frame update
    void Start()
    {
        NextPos = Positions[0];
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        if (transform.position == NextPos.position)
        {
            NextPosIndex++;

            if(NextPosIndex >= Positions.Length)
            {
                NextPosIndex = 0;
            }

            NextPos = Positions[NextPosIndex];
        }

        else
        {
            transform.position = Vector2.MoveTowards(transform.position, NextPos.position, speed * Time.deltaTime);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().Muerte();
        }
    }
}
