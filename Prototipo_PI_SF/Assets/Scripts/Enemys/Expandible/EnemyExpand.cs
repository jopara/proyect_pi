using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExpand : MuerteIntermitente
{
    public int damage = 1;
    private GameObject player;
    public Animator anim;

    public float tiempoExpansion = 3f;
    public float factorEscala = 5f;

    private float TiempoTranscurrido;
    private Vector3 initialScale;
    private Vector3 scaleMax;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        initialScale = new Vector3(transform.localScale.x , transform.localScale.y);
        scaleMax = initialScale * factorEscala;
        TiempoTranscurrido = 0f;
    }

    private void Update()
    {
        if (TiempoTranscurrido < tiempoExpansion)
        {
            TiempoTranscurrido += Time.deltaTime;
            transform.localScale = Vector3.Lerp(initialScale, scaleMax, TiempoTranscurrido / tiempoExpansion);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Knockback>().DoKnockback();
            collision.GetComponent<PlayerController>().Danyos(damage);


        }
    }

    public void MuerteNew()
    {
        anim.SetTrigger("Muerte");
        StartCoroutine(Wait());
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.35f);
        Destroy(gameObject);
    }
}
