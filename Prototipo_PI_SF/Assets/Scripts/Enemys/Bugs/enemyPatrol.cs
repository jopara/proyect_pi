using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyPatrol : MuerteIntermitente
{
    
    public float walkSpeed;
    public int damage = 1;
    private bool death = false;
    private PlayerController player;




    public Transform[] Positions;

    int NextPosIndex;
    Transform NextPos;



    // Start is called before the first frame update
    void Start()
    {
        if (player == null) player = GameObject.FindObjectOfType<PlayerController>();
        NextPos = Positions[0];
        FlipIfNeeded();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!death) Move();

    }

    private void Move()
    {
        if (Mathf.Abs(transform.position.x - NextPos.position.x) < 0.5f)
        {
            NextPosIndex++;

            if (NextPosIndex >= Positions.Length)
            {
                NextPosIndex = 0;
            }

            NextPos = Positions[NextPosIndex];
            FlipIfNeeded();
        }
        else
        {
            
            transform.position = Vector2.MoveTowards(transform.position, NextPos.position, walkSpeed * Time.deltaTime);

        }
    }

    //Flips the character if needed
    public void FlipIfNeeded()
    {
        if (NextPos.position.x - transform.position.x < 0f)
        {
            //Left
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            //Right
            this.transform.localScale = Vector3.one;
        }
    }

   
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && player.isDead == false)
        {
            collision.gameObject.GetComponent<Knockback>().DoKnockback();
            collision.GetComponent<PlayerController>().Danyos(damage);
            
            

        }
    }
}
