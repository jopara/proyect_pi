using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bola : MonoBehaviour
{
    public float velocidad;
    public float tiempoDeVida;
    

    public int damage = 1;
    public Rigidbody2D rb;

    

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.up * velocidad;
        Invoke("destruirBala", tiempoDeVida);
    }

    

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Knockback>().DoKnockback();
            collision.GetComponent<PlayerController>().Danyos(damage);
            DestruirBala();
            
        }

        else if (collision.gameObject.CompareTag("destruir"))
        {
            
            DestruirBala();
            
        }
    }

    public void DestruirBala()
    {
        Destroy(gameObject);
    }
}
