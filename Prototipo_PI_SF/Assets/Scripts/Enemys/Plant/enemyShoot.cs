using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyShoot : MonoBehaviour
{
    public int damage = 1;
    public float timer;
    public float maxTimer;
    public Animator anim;

    public Transform puntoDisparo;
    public Transform puntoDisparo2;
    public GameObject bola;
    public GameObject effect;


    // Update is called once per frame
    void Update()
    {
        timer = timer + Time.deltaTime;
        if (timer > maxTimer)
        {
            Disparo();
            timer = 0f;
        }
    }

    public void Disparo()
    {
        
        Instantiate(bola, puntoDisparo.position, transform.rotation);
        Instantiate(effect, puntoDisparo2.position, transform.rotation);
        anim.SetTrigger("Disparo");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            collision.gameObject.GetComponent<PlayerController>().Muerte();
        }
    }
}
