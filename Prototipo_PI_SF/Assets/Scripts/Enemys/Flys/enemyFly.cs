using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyFly : MuerteIntermitente
{
    public float speed;
    public int damage;
    private GameObject player;
    public bool chase = false;
    //public Transform startingPoint;
    public float timer;
    public float maxTimer;
    public bool mustPatrol;
    private bool death = false;

    public Animator anim;

    public Transform[] Positions;
    int NextPosIndex;
    Transform NextPos;

    // Start is called before the first frame update
    void Start()
    {
        mustPatrol = true;
        player = GameObject.FindGameObjectWithTag("Player");
        NextPos = Positions[NextPosIndex];
    }

    // Update is called once per frame
    void Update()
    {
        timer = timer + Time.deltaTime;

        if (player == null)
            return;


        if (mustPatrol && !death)
        {
            Patrol();
        }


        else if (chase == true)
        {
            mustPatrol = false;
            Chase();
        }
        else
        {
            mustPatrol = true;
        }

        Flip();
    }

    public void Patrol()
    {
        if (transform.position == NextPos.position)
        {
            NextPosIndex++;
            NextPosIndex %= Positions.Length;

            NextPos = Positions[NextPosIndex];
            FlipIfNeeded();
        }
        else
        {
            
            transform.position = Vector2.MoveTowards(transform.position, NextPos.position, speed * Time.deltaTime);

        }
    }

    private void Chase()
    {
        
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        if (Vector2.Distance((Vector2)transform.position, (Vector2) player.transform.position) <= 5f && timer > maxTimer)
        {
            
            anim.SetTrigger("enemyAttack");
            timer = 0f;
        }
        else
        {
            return;
        }
    }

    /*private void ReturnStartPoint()
    {
        transform.position = Vector2.MoveTowards(transform.position, startingPoint.position, speed * Time.deltaTime);
    }*/

    public void FlipIfNeeded()
    {
        if (NextPos.position.x - transform.position.x < 0f)
        {
            //Left
            //this.transform.localScale = new Vector3(-1f, 1f, 1f);
            this.transform.localScale = Vector3.one;
        }
        else
        {
            //Right
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
            //this.transform.localScale = Vector3.one;
        }
    }

    private void Flip()
    {
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Knockback>().DoKnockback();
            
            collision.GetComponent<PlayerController>().Danyos(damage);
        }
    }

    
}
