using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseControls : MonoBehaviour
{
    public enemyFly[] enemyArray;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            foreach (enemyFly enemy in enemyArray)
            {
                enemy.chase = true;
                enemy.mustPatrol = false;
            }
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            foreach (enemyFly enemy in enemyArray)
            {
                enemy.chase = false;
            }
        }
    }
}
