using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulso : MonoBehaviour
{
    public float attackRange;
    public Transform attackPoint;
    private ControlesJugador entrada;
    public GameManager gameManager;
    public GameObject pulsoAnim;
    
    public LayerMask wallLayers;
    
    public LayerMask enemyLayers;
    public float timer;
    public float maxTimer;
    //public Animator animator;



    private void Start()
    {
        
    }
    private void Awake()
    {
        entrada = new ControlesJugador();
        entrada.Enable();
        gameManager.controles = entrada;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer = timer + Time.deltaTime;
        if (timer > maxTimer)
        {
            if (entrada.controlesJugador.pulso.ReadValue<float>() > 0f)
            {
                Ataque();
                timer = 0f;
                 
            }
        }
    }

    public void Ataque()
    {
        Instantiate(pulsoAnim, attackPoint.position, transform.rotation);

        Collider2D[] enemigosGolpeados = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
       
        foreach (Collider2D enemigo in enemigosGolpeados)
        {
            if(enemigo.tag == "EnemyE")
            {
                enemigo.GetComponent<EnemyExpand>().MuerteNew();
            }
            else if (enemigo.tag == "EnemyB")
            {
                enemigo.GetComponent<MuerteIntermitente>().Muerte();
            }
            else if (enemigo.tag == "EnemyH")
            {
                enemigo.GetComponent<MuerteIntermitente>().Muerte();
            }
            else if (enemigo.tag == "EnemyF")
            {
                enemigo.GetComponent<MuerteIntermitente>().Muerte();
            }
            else if (enemigo.tag == "pared")
            {
                enemigo.GetComponent<ParedDestructible>().Destruccion();
            }
            else if (enemigo.tag == "bala")
            {
                enemigo.GetComponent<bola>().DestruirBala();
            }

        }

        

    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null) return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    


}
