using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesNuevos : PhysicsObject
{
    public float maxSpeed = 10f;
    public float jumpTakeOffSpeed = 7.5f;
    public int health;

    public GameManager gameManager;
    private ControlesJugador entrada;
    public BarraDeVida barraDeVida;

    private bool apretandoSalto = false;
    private bool puedehablar;
    public bool canMove = true;

    public void Awake()
    {
        entrada = new ControlesJugador();
        entrada.Enable();
        gameManager.controles = entrada;
        
    }

    void Start()
    {
        
        canMove = true;
    }

    // Update is called once per frame
    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        if (canMove)
        {
            if (entrada.controlesJugador.moverse.ReadValue<float>() != 0f)
            {
                move.x = entrada.controlesJugador.moverse.ReadValue<float>();
            }

            if (!apretandoSalto && grounded && entrada.controlesJugador.salto.ReadValue<float>() > 0f)
            {

                velocity.y = jumpTakeOffSpeed;


            }

            else if (apretandoSalto && entrada.controlesJugador.salto.ReadValue<float>() == 0f)
            {
                apretandoSalto = false;

                if (velocity.y > 0 && !grounded)
                {
                    velocity.y = velocity.y * 0.5f;
                }

            }

            if (entrada.controlesJugador.salto.ReadValue<float>() > 0f)
            {
                apretandoSalto = true;
            }
        }


        targetVelocity = move * maxSpeed;

    }

    public void Danyos(int damage)
    {
        health -= damage;

        barraDeVida.FuncionVida();

        if (health <= 0)
        {
            Muerte();
        }
    }

    public void Muerte()
    {
        Destroy(gameObject);
    }

    
}
