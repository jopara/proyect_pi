using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{


    private ControlesJugador entrada; //Declara al Input System
    private Rigidbody2D rb; // Declara al rigidbody del personaje
    public GameManager gameManager=null; // Declara al script de GameManager
    public SpriteRenderer spriteRenderer; // Declara al SpriteRenderer del sprite dentro del GameObject
    public BarraDeVida barraDeVida; //Declara al script de Barra de Vida
    public RespawnManager rm; //Declara al script que se encarga de los respawns
    public bool canMove = true; //Indica si el jugador se queda bloqueado (false) o si se puede desplazar (true)
    public bool invencible = false;
    public bool isDead = false;
    public Knockback knockback;
    public Animator animator;
    private Material matRed;
    private Material matDefault;

    //GESTION JOYSTICK
    private Vector2 entradaMovimiento = new Vector2(); 
    private BotonTeclado botonSalto = new BotonTeclado();
    private BotonTeclado botonInteraccion = new BotonTeclado();


    private bool saltoApretadoSinSoltar = false;
    private bool saltoApretado = false;

    private bool accionApretado = false;
    
    //GESTION FISICAS
    private Vector3 posicionPies;
    public GameObject groundChecker;
    public bool grounded=false;
    public float fuerzaSalto;
    private Vector2 detectorDelante;
    private int cantidadSaltosRestantes = 0; //Cantidad de saltos que le quedan, para controlar el doble salto
    public float tiempoInvencible;


    private Npc npcActivo = null;

    private Collider2D gmaux = null;

    public float velocidadMaxima=5f;
    public float aceleracion=2.5f; //Cuantas unidades incrementamos por segundo
    public int health = 3;


    public void Awake()
    {
        entrada = new ControlesJugador();
        entrada.Enable();
        if (gameManager == null) gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.controles = entrada;

    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        detectorDelante = new Vector2(GetComponent<CapsuleCollider2D>().bounds.size.x * 0.6f, 0f);
        if(gameManager==null) gameManager = GameObject.FindObjectOfType<GameManager>();
        //registro botones en GameManager
        gameManager.AnyadirBoton(botonInteraccion);
        gameManager.AnyadirBoton(botonSalto);

        matRed = Resources.Load("RedFlash", typeof(Material)) as Material;
        matDefault = spriteRenderer.material;
        invencible = false;
        canMove = true;

        /*rm = GameObject.FindObjectOfType<RespawnManager>();
        transform.position = rm.lastCheckPointPos;*/
    }

    // Update is called once per frame
    void Update()
    {
        entradaMovimiento = Vector2.zero;
        
        if (canMove)
        {
            entradaMovimiento.x = entrada.controlesJugador.moverse.ReadValue<float>();


            /*  if (grounded)
              {
                  gmaux = Physics2D.OverlapCircle(transform.position + posicionPies, 0.5f, LayerMask.NameToLayer("suelo"));
              }*/



            if (entrada.controlesJugador.salto.ReadValue<float>() > 0f)
            {
                botonSalto.Apretar();
            }
            else
            {
                botonSalto.Soltar();
            }

            if (entrada.controlesJugador.interactuar.ReadValue<float>() > 0f)
            {
                botonInteraccion.Apretar();
            }
            else
            {
                botonInteraccion.Soltar();
            }
        }
        /*entradaMovimiento.x = entrada.controlesJugador.moverse.ReadValue<float>();

        
      



        if(entrada.controlesJugador.salto.ReadValue<float>() > 0f)
        {
            botonSalto.Apretar();
        }else
        {
            botonSalto.Soltar();
        }

        if (entrada.controlesJugador.interactuar.ReadValue<float>() > 0f)
        {
            botonInteraccion.Apretar();
        }
        else {
            botonInteraccion.Soltar();
        }*/



    }





    /*TODO:
     * Colisionador frena en paredes
     * Colision con enemigo dispara hacia abajo
     * NPC
     * Soltar salto debe decelerar al personaje
     * Doble salto
     * */

    private void FixedUpdate()
    {

        //DETECTAMOS SUELO
        Collider2D[] raycasts = Physics2D.OverlapCircleAll((Vector2)(groundChecker.transform.position),0.05f);
        grounded = false;
        foreach (Collider2D coll in raycasts)
        {
            if (LayerMask.LayerToName(coll.gameObject.layer) == "suelo")
            {
                grounded = true;
                Debug.Log("Grounded");
            }
        }

        //Acciones
        if (botonInteraccion.RecienApretado()) {

            //ya estoy hablando?
            if (ConversacionManejador.EstaDialogando()) {

                if (ConversacionManejador.TieneOpciones())
                {

                }
                else
                {
                    //llamo a siguiente frase
                    ConversacionManejador.instancia.onClickOpcion1();

                }

            } else {
                //compruebo si es un NPC
                Collider2D[] colliders = Physics2D.OverlapPointAll(((Vector2)transform.position) + detectorDelante);
                Collider2D NPCdetectado = null;

                foreach (Collider2D collider in colliders)
                {
                    if (collider.CompareTag("NPC"))
                    {
                        NPCdetectado = collider;
                    }
                }
                if (NPCdetectado!=null)
                {
                    //comenzamos a hablar
                    NPCdetectado.GetComponent<Npc>().Hablar();
                }
            }

        }

        //bool canMove = !ConversacionManejador.EstaDialogando() && /*Portal */ true; 


        
        

            Vector2 nuevaVelocidad = rb.velocity;
            if (entradaMovimiento.x == 0f)
            {
                nuevaVelocidad.x *= 0.5f;
                if (Mathf.Abs(nuevaVelocidad.x) < 0.1f) nuevaVelocidad.x = 0f;
            }
            else
            {
                
                nuevaVelocidad.x += entradaMovimiento.x * aceleracion;
                if (nuevaVelocidad.x > velocidadMaxima) nuevaVelocidad.x = velocidadMaxima;
                if (nuevaVelocidad.x < -velocidadMaxima) nuevaVelocidad.x = -velocidadMaxima;
            }

            //gesti�n salto y gravedad
            if (botonSalto.RecienApretado())
            {
                if (grounded)
                {
                    //APRETAMOS SALTO POR PRIMERA VEZ

                    //rb.AddForce(Vector2.up * fuerzaSalto);
                    
                    nuevaVelocidad.y = fuerzaSalto;
                    cantidadSaltosRestantes = 1; //Si cambias este 1 a 2, el salto podr�a ser triple. Y as� sucesivamente
                    Debug.Log("Salto1");
                }
                else if (cantidadSaltosRestantes > 0 && GameManager.instance.dobleSaltoActivado)
                {
                    Debug.Log("Salto2");
                    cantidadSaltosRestantes--;
                    nuevaVelocidad.y = fuerzaSalto; //Se puede cambiar por nuevaVelocidad.y = nuevaVelocidad.y + fuerzaSalto; De esa manera, el salto se acumula y salta mucho m�s alto }

                }
            }

            if (botonSalto.RecienSoltado())
            {
                //frenamos el salto en cuanto suelta el bot�n de salto
                if (nuevaVelocidad.y > 0 && !grounded)
                {
                    nuevaVelocidad.y = nuevaVelocidad.y * 0.5f;
                }

            }

            rb.velocity = nuevaVelocidad;
            //Manejo giro sprite seg�n direcci�n
            if (nuevaVelocidad.x > 0.1f)
            {
                //flip desactivado porque miramos a la derecha
                spriteRenderer.flipX = false;
            }
            else if (nuevaVelocidad.x < -0.1f)
            {
                //flip activado porque miramos a la izquierda
                spriteRenderer.flipX = true;
            }
            animator.SetBool("moving", Mathf.Abs(nuevaVelocidad.x) != 0f);
            animator.SetBool("grounded", grounded);
            animator.SetFloat("velocity.y", nuevaVelocidad.y);
        

        gameManager.ActualizarBotones();


    }


    public void Danyos(int damage)
    {
        if (!invencible)
        {
            
            animator.SetTrigger("playerhurt");
            health -= damage;
            //spriteRenderer.material = matRed;
            invencible = true;
            barraDeVida.FuncionVida();

            if (health <= 0)
            {
                isDead = true;
                Muerte();
                StartCoroutine(ResetInvencible());
            }

            else
            {
                StartCoroutine(ResetInvencible());
            }
        }
        
        /*else if(invencible)
        {
            StartCoroutine(ResetInvencible());
        }*/
    }

    public void ResetMaterial()
    {
        spriteRenderer.material = matDefault;
    }

    public IEnumerator ResetInvencible()
    {
        yield return new WaitForSeconds(tiempoInvencible);
        invencible = false;
    }

    public void Muerte()
    {
        canMove = false;
        knockback.playerDead = true;
        animator.SetTrigger("playerdead");
        StartCoroutine(MovePlayer());
        //Destroy(gameObject);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public IEnumerator MovePlayer()
    {
        yield return new WaitForSeconds(1.25f);
        canMove = true;
        isDead = false;
        knockback.playerDead = false;
        transform.position = rm.lastCheckPointPos;
        health = 3;
    }
}
