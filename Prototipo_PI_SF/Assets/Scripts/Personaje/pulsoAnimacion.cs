using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pulsoAnimacion : MonoBehaviour
{
    public Animator anim;
    public float tiempoDeVida;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetTrigger("attackPulse");
        Invoke("destruir", tiempoDeVida);
    }

    public void Destruir()
    {
        Destroy(gameObject);
    }
    
}
