// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Personaje/controlesJugador.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @ControlesJugador : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @ControlesJugador()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""controlesJugador"",
    ""maps"": [
        {
            ""name"": ""controlesJugador"",
            ""id"": ""1a6ff131-1ead-4f4f-b41a-085372a3922b"",
            ""actions"": [
                {
                    ""name"": ""moverse"",
                    ""type"": ""PassThrough"",
                    ""id"": ""eb729f88-134d-4000-a4e8-78925cb4491e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""salto"",
                    ""type"": ""PassThrough"",
                    ""id"": ""e2fa0808-6517-40d0-ba51-009585da43fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""pulso"",
                    ""type"": ""PassThrough"",
                    ""id"": ""978762e9-1679-452c-869d-279491dd9ca8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""interactuar"",
                    ""type"": ""PassThrough"",
                    ""id"": ""bd6cc8fd-d435-4ea1-aa81-71a0dbdec000"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""pausa"",
                    ""type"": ""Button"",
                    ""id"": ""7815e7ae-22ee-4686-b917-d43b43256c49"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""07feaa77-fcca-4dd9-accf-2e345ca84b2e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""salto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7ce8dbba-f0fd-44ff-8333-545fd4848cc1"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""salto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""caa40217-9db8-4d46-872e-0afbb602bfdf"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""salto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f960799a-a01b-4792-9d9c-9091ae0e361d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""pulso"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9d2ac82-16db-415b-9584-d014b48aac59"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""pulso"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6327b43a-cc8b-4885-9587-5fb60fc95793"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""interactuar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a9aea00-b7e7-4b62-926a-3475aa9488be"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""interactuar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""21b650c0-3bf9-4f71-8856-bd01d2e36408"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""300e4a53-3a09-4fee-b95d-edadc56bafc9"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""f36ffe1e-ff29-4748-b6d2-a9ba2bef1ef7"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""63eac685-d1f5-4004-9b00-40bcac625b06"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""43cf3eb9-1ffc-4432-957b-9583bbf8bbd8"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a4cf0420-2aa1-40ac-974b-b570f2f4990c"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""f4a8c24a-a502-478f-9211-49762e3ec89d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""aa3365da-daee-4668-86d8-b0b131590cc2"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a288b896-9bf3-456b-aefa-61c2281baa5f"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""moverse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""e6eb9765-31c4-4f94-bea1-a6e3fe5aa4cd"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""pausa"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // controlesJugador
        m_controlesJugador = asset.FindActionMap("controlesJugador", throwIfNotFound: true);
        m_controlesJugador_moverse = m_controlesJugador.FindAction("moverse", throwIfNotFound: true);
        m_controlesJugador_salto = m_controlesJugador.FindAction("salto", throwIfNotFound: true);
        m_controlesJugador_pulso = m_controlesJugador.FindAction("pulso", throwIfNotFound: true);
        m_controlesJugador_interactuar = m_controlesJugador.FindAction("interactuar", throwIfNotFound: true);
        m_controlesJugador_pausa = m_controlesJugador.FindAction("pausa", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // controlesJugador
    private readonly InputActionMap m_controlesJugador;
    private IControlesJugadorActions m_ControlesJugadorActionsCallbackInterface;
    private readonly InputAction m_controlesJugador_moverse;
    private readonly InputAction m_controlesJugador_salto;
    private readonly InputAction m_controlesJugador_pulso;
    private readonly InputAction m_controlesJugador_interactuar;
    private readonly InputAction m_controlesJugador_pausa;
    public struct ControlesJugadorActions
    {
        private @ControlesJugador m_Wrapper;
        public ControlesJugadorActions(@ControlesJugador wrapper) { m_Wrapper = wrapper; }
        public InputAction @moverse => m_Wrapper.m_controlesJugador_moverse;
        public InputAction @salto => m_Wrapper.m_controlesJugador_salto;
        public InputAction @pulso => m_Wrapper.m_controlesJugador_pulso;
        public InputAction @interactuar => m_Wrapper.m_controlesJugador_interactuar;
        public InputAction @pausa => m_Wrapper.m_controlesJugador_pausa;
        public InputActionMap Get() { return m_Wrapper.m_controlesJugador; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ControlesJugadorActions set) { return set.Get(); }
        public void SetCallbacks(IControlesJugadorActions instance)
        {
            if (m_Wrapper.m_ControlesJugadorActionsCallbackInterface != null)
            {
                @moverse.started -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnMoverse;
                @moverse.performed -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnMoverse;
                @moverse.canceled -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnMoverse;
                @salto.started -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnSalto;
                @salto.performed -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnSalto;
                @salto.canceled -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnSalto;
                @pulso.started -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPulso;
                @pulso.performed -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPulso;
                @pulso.canceled -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPulso;
                @interactuar.started -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnInteractuar;
                @interactuar.performed -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnInteractuar;
                @interactuar.canceled -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnInteractuar;
                @pausa.started -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPausa;
                @pausa.performed -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPausa;
                @pausa.canceled -= m_Wrapper.m_ControlesJugadorActionsCallbackInterface.OnPausa;
            }
            m_Wrapper.m_ControlesJugadorActionsCallbackInterface = instance;
            if (instance != null)
            {
                @moverse.started += instance.OnMoverse;
                @moverse.performed += instance.OnMoverse;
                @moverse.canceled += instance.OnMoverse;
                @salto.started += instance.OnSalto;
                @salto.performed += instance.OnSalto;
                @salto.canceled += instance.OnSalto;
                @pulso.started += instance.OnPulso;
                @pulso.performed += instance.OnPulso;
                @pulso.canceled += instance.OnPulso;
                @interactuar.started += instance.OnInteractuar;
                @interactuar.performed += instance.OnInteractuar;
                @interactuar.canceled += instance.OnInteractuar;
                @pausa.started += instance.OnPausa;
                @pausa.performed += instance.OnPausa;
                @pausa.canceled += instance.OnPausa;
            }
        }
    }
    public ControlesJugadorActions @controlesJugador => new ControlesJugadorActions(this);
    public interface IControlesJugadorActions
    {
        void OnMoverse(InputAction.CallbackContext context);
        void OnSalto(InputAction.CallbackContext context);
        void OnPulso(InputAction.CallbackContext context);
        void OnInteractuar(InputAction.CallbackContext context);
        void OnPausa(InputAction.CallbackContext context);
    }
}
