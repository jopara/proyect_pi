using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum faseBoton { 
    Suelto,
    RecienApretado,
    Apretado,
    RecienSoltado
}

public class BotonTeclado
{

    faseBoton fase = faseBoton.Suelto;

    public void Apretar()
    {
        if (fase == faseBoton.Suelto || fase == faseBoton.RecienSoltado)
        {
            fase = faseBoton.RecienApretado;
        }
    }

    public void Soltar()
    {
        if (fase == faseBoton.Apretado || fase == faseBoton.RecienApretado)
        {
            fase = faseBoton.RecienSoltado;
        }
    }

    public bool RecienApretado() { return fase == faseBoton.RecienApretado; }
    public bool RecienSoltado() { return fase == faseBoton.RecienSoltado; }

    public void Actualizar()
    {
            if (fase == faseBoton.RecienApretado)
            {
                fase = faseBoton.Apretado;
            }

            if (fase == faseBoton.RecienSoltado)
            {
                fase = faseBoton.Suelto;
            }
    }
}
