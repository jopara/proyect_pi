using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPos : MonoBehaviour
{
    public RespawnManager rm;
    // Start is called before the first frame update
    void Start()
    {
        rm = GameObject.FindObjectOfType<RespawnManager>();
        transform.position = rm.lastCheckPointPos;
    }

   
}
