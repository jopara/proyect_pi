using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knockback : MonoBehaviour
{

    public float knockbackLength;
    public float knockbackForce;
    public bool playerDead = false;

    public PlayerController controlesPlayer;
    public Rigidbody2D rb;

    public bool isHurt;

    // Start is called before the first frame update
    void Start()
    {
        controlesPlayer = GetComponent<PlayerController>();
        rb = GetComponent<Rigidbody2D>();
        playerDead = false;
    }

    public void DoKnockback()
    {
        if (playerDead == false)
        {
            StartCoroutine(DisablePlayerMovement(knockbackLength));
            Vector2 vecAuxiliar = new Vector2(-rb.velocity.x, -rb.velocity.y);

            rb.velocity = vecAuxiliar.normalized * knockbackForce;
            //rb.AddForce = new Vector2(-controlesPlayer.transform.position.x * knockbackForce, knockbackForce);
        }

        else if (playerDead == true)
        {
            return;
        }
        /*StartCoroutine(DisablePlayerMovement(knockbackLength));
        Vector2 vecAuxiliar = new Vector2(-rb.velocity.x, -rb.velocity.y);
        
        rb.velocity = vecAuxiliar.normalized * knockbackForce;
        //rb.AddForce = new Vector2(-controlesPlayer.transform.position.x * knockbackForce, knockbackForce);*/

    }

    IEnumerator DisablePlayerMovement(float time)
    {
        controlesPlayer.canMove = false;
        isHurt = true;
        yield return new WaitForSeconds(time);
        controlesPlayer.canMove = true;
        isHurt = false;
    }



}
