using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    private GameManager gameManager = null;
    private ControlesJugador entrada;
    public GameObject canvasPausa;
    public GameObject canvasGui;
    public GameObject canvasControls;
    public GameObject canvasTeclado;
    public GameObject canvasMando;

    // Start is called before the first frame update
    void Awake()
    {
        entrada = new ControlesJugador();
        entrada.Enable();
        if (gameManager == null) gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.controles = entrada;

        canvasPausa.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(entrada.controlesJugador.pausa.ReadValue<float>() > 0f)
        {
            Pausa();
        }
    }

    public void Pausa()
    {
        GameManager.instance.pausa = true;
        canvasGui.SetActive(false);
        canvasPausa.SetActive(true);
    }
    public void CanvasExit()
    {
        canvasControls.SetActive(false);
        canvasPausa.SetActive(true);
    }

    public void Controls()
    {
        canvasPausa.SetActive(false);
        canvasControls.SetActive(true);
        Debug.Log("controles");
    }

    public void Teclado()
    {
        canvasTeclado.SetActive(true);
        canvasMando.SetActive(false);
    }

    public void Mando()
    {
        canvasTeclado.SetActive(false);
        canvasMando.SetActive(true);
    }

    public void Cerrar()
    {
        GameManager.instance.pausa = false;
        canvasGui.SetActive(true);
        canvasPausa.SetActive(false);
    }

    public void Salir()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

}
