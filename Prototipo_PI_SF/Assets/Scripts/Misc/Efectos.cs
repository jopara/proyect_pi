using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Efectos : MonoBehaviour
{
    public float tiempoDeVida;

    // Start is called before the first frame update
    private void Awake()
    {
        Invoke("Destruir", tiempoDeVida);
    }

    private void Destruir()
    {
        Destroy(gameObject);
    }
}
