using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ObjetosEntregables { 
    Photo,
    BugCage,
    Blanket,
    TeddyBear
}


public class ObjetoInteractuable : MonoBehaviour
{

    public ObjetosEntregables objeto;

    public GameObject canvasObjeto;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.instance.CojoObjeto(objeto);
            canvasObjeto.SetActive(true);
            Destroy(gameObject);
        }
    }
}
