using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum IdDialogos { 
   
    Null,
    BugsOpening,
    BugsAskEnemies,
    BugsHowHelp,
    BugsAfterPhoto,
    BugsAfterBug,
    BugsFinalWords,
    BugsNoPhoto,
    BugsNoBug,
    HungerOpening,
    HungerAskEnemies,
    HungerHowHelp,
    HungerAfterBlanket,
    HungerAfterTeddyBear,
    HungerFinalWords,
    HungerNoBlanket,
    HungerNoTeddyBear,
}


public class ConversacionManejador : MonoBehaviour
{
    public static ConversacionManejador instancia;
    


    //Referencias
    private Canvas canvas;
    private Text textoDialogo;
    private Button opcion1;
    private Button opcion2;
    private Button opcion3;
    private Button opcion4;
    public Button salir;

    Text textOpcion1;
    Text textOpcion2;
    Text textOpcion3;
    Text textOpcion4;

    [SerializeField] static IdDialogos idOpcion1;
    [SerializeField] IdDialogos idOpcion2;
    [SerializeField] IdDialogos idOpcion3;
    [SerializeField] IdDialogos idOpcion4;

    static int idActual=-1;


    public static bool EstaDialogando() { return idActual != -1; }

    public static bool TieneOpciones() { return idOpcion1!=IdDialogos.Null; }

    private void Start()
    {
        instancia = this;
        GameManager.instance.canvasDialogoBichos.enabled = false;
        GameManager.instance.canvasDialogoHambre.enabled = false;
        
        /*
        textOpcion1 = opcion1.GetComponentInChildren<Text>();
        textOpcion2 = opcion2.GetComponentInChildren<Text>();
        textOpcion3 = opcion3.GetComponentInChildren<Text>();
        textOpcion4 = opcion4.GetComponentInChildren<Text>();
        */
        

    }
     
    public void iniciarDialogo(IdDialogos idDeInicio) {

        if (idDeInicio == IdDialogos.BugsOpening)
        {
            //Dialogo bichos
            canvas = GameManager.instance.canvasDialogoBichos;
            GameManager.instance.canvasDialogoBichos.enabled = true;
            GameManager.instance.canvasDialogoHambre.enabled = false;
            //SoundManager.PlaySound("NPCBugs");
        }
        else {
            //Dialogo hambre
            canvas = GameManager.instance.canvasDialogoHambre;
            GameManager.instance.canvasDialogoHambre.enabled = true;
            GameManager.instance.canvasDialogoBichos.enabled = false;
            //SoundManager.PlaySound("NPCHunger");
        }

        canvas.enabled = true;
        instancia = this;
        textoDialogo = canvas.transform.Find("ImageText").transform.GetChild(0).GetComponent<Text>();
        opcion1 = canvas.transform.Find("PanelRespuestas").Find("Button").GetComponent<Button>(); 
        textOpcion1 = opcion1.GetComponentInChildren<Text>();
        opcion2 = canvas.transform.Find("PanelRespuestas").Find("Button2").GetComponent<Button>(); 
        textOpcion2 = opcion2.GetComponentInChildren<Text>();
        opcion3 = canvas.transform.Find("PanelRespuestas").Find("Button3").GetComponent<Button>();
        textOpcion3 = opcion3.GetComponentInChildren<Text>();
        opcion4 = canvas.transform.Find("PanelRespuestas").Find("Button4").GetComponent<Button>();
        textOpcion4 = opcion4.GetComponentInChildren<Text>();
        

        cargarDialogo(idDeInicio);

    }
        
    public void cargarDialogo(IdDialogos id) {


        switch (id)
        {
            #region SWITCH BUG
            case IdDialogos.BugsOpening:
                #region BugsOpening
                if (GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Photo) && GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.BugCage)) {
                    cargarDialogo(IdDialogos.BugsFinalWords);
                    return;
                }

                textoDialogo.text = "The bugs!The bugs are everywhere! I can hear them and feel them moving around! Help! Someone! Anyone! I don't want to be alone!";
                textOpcion1.text = "Ask about the enemies";
                textOpcion1.enabled = true;
                idOpcion1 = IdDialogos.BugsAskEnemies;
                textOpcion2.text = "Ask how to help them";
                textOpcion2.enabled = true;
                idOpcion2 = IdDialogos.BugsHowHelp;

                if (!GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Photo) && !GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.BugCage))
                {

                    textOpcion3.text = "Give them the Photo";
                    textOpcion3.enabled = true;

                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.Photo))
                    {
                        idOpcion3 = IdDialogos.BugsAfterPhoto;
                    }
                    else {
                        idOpcion3 = IdDialogos.BugsNoPhoto;
                    }
                    textOpcion4.text = "Give them the Bug cage";
                    textOpcion4.enabled = true;
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.BugCage))
                    {
                        idOpcion4 = IdDialogos.BugsAfterBug;
                    }
                    else
                    {
                        idOpcion4 = IdDialogos.BugsNoBug;
                    }
                    

                }
                else if (!GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Photo))
                {

                    textOpcion3.text = "Give them the Photo";
                    textOpcion3.enabled = true;
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.Photo))
                    {
                        idOpcion3 = IdDialogos.BugsAfterPhoto;
                    }
                    else
                    {
                        idOpcion3 = IdDialogos.BugsNoPhoto;
                    }
                    //textOpcion4.text = "Give them the Bug cage";
                    textOpcion4.enabled = false;
                    idOpcion4 = IdDialogos.Null;
                }
                else {
                    textOpcion3.text = "Give them the Bug cage";
                    textOpcion3.enabled = true; 
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.BugCage))
                    {
                        idOpcion3 = IdDialogos.BugsAfterBug;
                    }
                    else
                    {
                        idOpcion3 = IdDialogos.BugsNoBug;
                    }
                    //textOpcion4.text = "Give them the Bug cage";
                    textOpcion4.enabled = false;
                    idOpcion4 = IdDialogos.Null;
                }
                #endregion
                break;
            case IdDialogos.BugsAfterBug:
                #region BugsAfterBug
                GameManager.instance.objetosEntregados.Add(ObjetosEntregables.BugCage);
                GameManager.instance.objetosEnInventario.Remove(ObjetosEntregables.BugCage);

                textoDialogo.text = "A cage! Like the ones my aunt has! She uses them to catch bugs! I can catch them so they stop crawling around!";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsAfterPhoto:
                #region BugsAfterPhoto

                GameManager.instance.objetosEntregados.Add(ObjetosEntregables.Photo);
                GameManager.instance.objetosEnInventario.Remove(ObjetosEntregables.Photo);
                textoDialogo.text = "My aunt! She's so great! She knows so much about bugs and is so nice to me!She taught me how to handle big and small bugs! She's so nice!";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsAskEnemies:
                #region BugsAskEnemies
                textoDialogo.text = "There's so many and they're always crawling and buzzing and there's no one to stop them! I just want someone to stop them! I don't wanna be alone!";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsFinalWords:
                #region BugsFinalWords

                GameManager.instance.dobleSaltoActivado = true;

                textoDialogo.text = "You know. The bugs aren't as scary like this. I kind of like them! Thank you for helping me. You remind me of my aunt. You're nice. Here, this can help you! Now you'll jump high like a grasshopper";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.Null;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsHowHelp:
                #region BugsFinalWords
                textoDialogo.text = "I want my aunt! She knows how to help! She knows about bugs! She knows how to trap them! I want my aunt! Please!";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsNoBug:
                #region BugsNoBug
                
                textoDialogo.text = "Please, bring me a bug cage like the ones my aunt used";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.BugsNoPhoto:
                #region BugsNoPhoto
                
                textoDialogo.text = "I feel so lonely here... I miss my aunt...";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.BugsOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            #endregion

            
            case IdDialogos.Null:
                Salida();
                break;
        
        }
        switch (id)
        {
            #region SWITCH HUNGER
            case IdDialogos.HungerOpening:
                #region HungerOpening
                if (GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Blanket) && GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.TeddyBear))
                {
                    cargarDialogo(IdDialogos.HungerFinalWords);
                    return;
                }

                textoDialogo.text = "I'm scared and hungry... I want to eat but I'm scared that if I move I'll get hurt... Everything's so scary...";
                textOpcion1.text = "Ask about the enemies";
                textOpcion1.enabled = true;
                idOpcion1 = IdDialogos.HungerAskEnemies;
                textOpcion2.text = "Ask how to help them";
                textOpcion2.enabled = true;
                idOpcion2 = IdDialogos.HungerHowHelp;

                if (!GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Blanket) && !GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.TeddyBear))
                {

                    textOpcion3.text = "Give them the Blanket";
                    textOpcion3.enabled = true;

                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.Blanket))
                    {
                        idOpcion3 = IdDialogos.HungerAfterBlanket;
                    }
                    else
                    {
                        idOpcion3 = IdDialogos.HungerNoBlanket;
                    }
                    textOpcion4.text = "Give them the Teddy Bear";
                    textOpcion4.enabled = true;
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.TeddyBear))
                    {
                        idOpcion4 = IdDialogos.HungerAfterTeddyBear;
                    }
                    else
                    {
                        idOpcion4 = IdDialogos.HungerNoTeddyBear;
                    }


                }
                else if (!GameManager.instance.objetosEntregados.Contains(ObjetosEntregables.Blanket))
                {

                    textOpcion3.text = "Give them the Blanket";
                    textOpcion3.enabled = true;
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.Blanket))
                    {
                        idOpcion3 = IdDialogos.HungerAfterBlanket;
                    }
                    else
                    {
                        idOpcion3 = IdDialogos.HungerNoBlanket;
                    }
                    //textOpcion4.text = "Give them the Bug cage";
                    textOpcion4.enabled = false;
                    idOpcion4 = IdDialogos.Null;
                }
                else
                {
                    textOpcion3.text = "Give them the Teddy Bear";
                    textOpcion3.enabled = true;
                    if (GameManager.instance.objetosEnInventario.Contains(ObjetosEntregables.TeddyBear))
                    {
                        idOpcion3 = IdDialogos.HungerAfterTeddyBear;
                    }
                    else
                    {
                        idOpcion3 = IdDialogos.HungerNoTeddyBear;
                    }
                    //textOpcion4.text = "Give them the Bug cage";
                    textOpcion4.enabled = false;
                    idOpcion4 = IdDialogos.Null;
                }
                #endregion
                break;
            case IdDialogos.HungerAfterBlanket:
                #region HungerAfterBlanket
                GameManager.instance.objetosEntregados.Add(ObjetosEntregables.Blanket);
                GameManager.instance.objetosEnInventario.Remove(ObjetosEntregables.Blanket);

                textoDialogo.text = "My blanket! So soft and warm... It always makes me feel better. Helps me forget how hungry I am...";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerAfterTeddyBear:
                #region HungerAfterTeddyBear
                GameManager.instance.objetosEntregados.Add(ObjetosEntregables.TeddyBear);
                GameManager.instance.objetosEnInventario.Remove(ObjetosEntregables.TeddyBear);

                textoDialogo.text = "Teddy! He protects me when mama and papa are angry... He's really nice... He makes me feel safe.";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerAskEnemies:
                #region HungerAskEnemies
                textoDialogo.text = "The hands, there's hands everywhere and they won't let me eat... I just want to eat, I want to feel safe...";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerHowHelp:
                #region HungerHowHelp
                textoDialogo.text = "I want my teddy... He protects me from the monsters and keeps me warm... I always hide with him under my blanket... It's warm and safe... I like it...";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerFinalWords:
                #region HungerFinalWords

                GameManager.instance.SegundaMitadActivado = true;

                textoDialogo.text = "I feel safe here... I'm safe here. I don't feel hungry anymore, or scared. You're like my teddy, you make me feel safe... Thank you... Go back to the start, something's waiting for you there.";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.Null;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerNoBlanket:
                #region HungerNoBlanket
                textoDialogo.text = "When I put on my blanket I feel so warm... And I dont hear the shouts of papa and mama... ";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos. HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            case IdDialogos.HungerNoTeddyBear:
                #region HungerNoTeddyBear
                textoDialogo.text = "Teddy it's like my friend... It makes me feel safe...";
                textOpcion1.text = "Next";
                textOpcion2.text = "";
                textOpcion3.text = "";
                textOpcion4.text = "";
                textOpcion1.enabled = true;
                textOpcion2.enabled = false;
                textOpcion3.enabled = false;
                textOpcion4.enabled = false;
                idOpcion1 = IdDialogos.HungerOpening;
                idOpcion2 = IdDialogos.Null;
                idOpcion3 = IdDialogos.Null;
                idOpcion4 = IdDialogos.Null;
                #endregion
                break;
            #endregion

            case IdDialogos.Null:
                Salida();
                break;
        }

    }

    private void Salida()
    {
        canvas.enabled = false;
    }

    public void onClickOpcion1()
    {
        cargarDialogo(idOpcion1);
    }
    public void onClickOpcion2()
    {
        cargarDialogo(idOpcion2);
    }
    public void onClickOpcion3()
    {
        cargarDialogo(idOpcion3);
    }
    public void onClickOpcion4()
    {
        cargarDialogo(idOpcion4);
    }

    public void onClickSalir()
    {
        Salida();
    }



}
