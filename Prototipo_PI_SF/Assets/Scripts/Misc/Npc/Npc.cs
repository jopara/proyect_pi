using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Npc : MonoBehaviour
{
    //public string[] frases;
    private GameManager gameManager;

    public IdDialogos idInicioConversaciones;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Hablar()
    {
        if (!ConversacionManejador.EstaDialogando())
        {
            ConversacionManejador.instancia.iniciarDialogo(idInicioConversaciones);
        }
    }

}
