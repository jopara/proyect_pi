using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DefaultExecutionOrder(-1)]
public class GameManager : MonoBehaviour
{
    public ControlesJugador controles;
    public Canvas canvasDialogoBichos;
    public Canvas canvasDialogoHambre;
    public GameObject canvasFirstHalf;
    public GameObject canvasSecondHalf;
    public List<BotonTeclado> botonesTeclado = new List<BotonTeclado>();

    public bool tengoObjeto1, tengoObjeto2, tengoObjeto3, tengoObjeto4;
    public bool entregadoObjeto1, entregadoObjeto2, entregadoObjeto3, entregadoObjeto4;
    public bool pausa=false;

    public static GameManager instance = null;

    public List<ObjetosEntregables> objetosEnInventario = new List<ObjetosEntregables>();
    public List<ObjetosEntregables> objetosEntregados = new List<ObjetosEntregables>();



    //GESTION OBJETOS
    bool tieneObjeto1 = false;
    internal bool _dobleSaltoActivado;
    internal bool dobleSaltoActivado
    {
        get { return _dobleSaltoActivado; }
        set
        {
            _dobleSaltoActivado = true;
            //Todo: MANEJAR CANVAS MEDIO CIRCULO
            canvasFirstHalf.SetActive(true);
        }
    }

    internal bool _SegundaMitadActivado;
    internal bool SegundaMitadActivado
    {
        get { return _SegundaMitadActivado; }
        set
        {
            _SegundaMitadActivado = true;
            //Todo: MANEJAR CANVAS SEGUNDO MEDIO CIRCULO
            canvasSecondHalf.SetActive(true);
        }
    }

    internal void CojoObjeto(ObjetosEntregables objeto)
    {
        objetosEnInventario.Add(objeto);
    }

    internal bool ObjetoEnInventario(ObjetosEntregables objeto)
    {
        return objetosEnInventario.Contains(objeto);
    }

    internal void EntregoObjeto(ObjetosEntregables objeto)
    {
        objetosEnInventario.Remove(objeto);
        objetosEntregados.Add(objeto);
    }


    // Start is called before the first frame update
    void Start()
    {
        if (instance == null) instance = this;

        tengoObjeto1 = tengoObjeto2 = tengoObjeto3 = tengoObjeto4 = false;
        entregadoObjeto1 = entregadoObjeto2 = entregadoObjeto3 = entregadoObjeto4 = false;

        Application.targetFrameRate = 24;
    }

    private void Update()
    {
        if (pausa)
        {
            Time.timeScale = 0f;
        }

        else
        {
            Time.timeScale = 1f;
        }
    }

    public void ActualizarBotones()
    {
        //Actualización de los botones de teclado
        foreach (BotonTeclado bt in botonesTeclado)
        {
            bt.Actualizar();
        }
    }


    internal void AnyadirBoton(BotonTeclado botonTeclado)
    {

        botonesTeclado.Add(botonTeclado);
    }
}
