using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteIntermitente : MonoBehaviour
{

    private Vector3 posicionOriginal;
    private static float tiempoDesactivado = 5f;
    private static float tiempoAnimacionMuerte = 0.5f;
    private Collider2D triggerCollider;

    public void Muerte() {

        StartCoroutine(CorrutinaMuerte());
    }

    private IEnumerator CorrutinaMuerte()
    {
        //Animacion muerte
        this.GetComponent<Animator>().SetBool("Muerte", true);
        triggerCollider = this.GetComponent<Collider2D>();
        triggerCollider.enabled = false;
        yield return new WaitForSeconds(tiempoAnimacionMuerte);



        //"desactivarlo"
        posicionOriginal = transform.position;
        transform.position = Vector3.one * 999999;

        this.GetComponent<Animator>().SetBool("Muerte", false);

        //Esperar x tiempo
        yield return new WaitForSeconds(tiempoDesactivado);

        //"activarlo"
        transform.position = posicionOriginal;
        triggerCollider.enabled = true;
    }





}
