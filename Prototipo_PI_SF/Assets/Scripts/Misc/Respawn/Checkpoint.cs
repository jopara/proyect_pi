using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private RespawnManager rm;

    private void Start()
    {
        rm = GameObject.FindObjectOfType<RespawnManager>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            rm.lastCheckPointPos = transform.position;
        }
    }

}
