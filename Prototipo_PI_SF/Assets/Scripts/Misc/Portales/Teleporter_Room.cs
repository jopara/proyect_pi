using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter_Room : MonoBehaviour
{
    public GameObject portal;
    public GameObject player;
    



    public float tiempo = 0.5f;


    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Teleport());
        }
    }

    IEnumerator Teleport()
    {
        yield return new WaitForSeconds(tiempo);
        
        player.transform.position = new Vector2(portal.transform.position.x, portal.transform.position.y);
    }
}
