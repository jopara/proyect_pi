using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalFinal : MonoBehaviour
{
    public GameManager gameManager = null;
    private PlayerController player = null;
    public GameObject canvasTPFinal;

    // Start is called before the first frame update
    void Start()
    {
        if (gameManager == null) gameManager = GameObject.FindObjectOfType<GameManager>();
        if (player == null) player = GameObject.FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            /*if(entrada.controlesJugador.interactuar.ReadValue<float>() > 0f)
            {
                canvasTP.SetActive(true);
                py.canMove = false;
                portalDestino = portal;
            }*/

            if(GameManager.instance._dobleSaltoActivado == true && GameManager.instance._SegundaMitadActivado == true)
            {
                player.canMove = false;
                canvasTPFinal.SetActive(true);

            }
            
        }
    }

    public void Cerrar()
    {
        canvasTPFinal.SetActive(false);
        player.canMove = true;
    }

    public void Teleport()
    {

        SceneManager.LoadScene("Scenes/O");
    }

}
