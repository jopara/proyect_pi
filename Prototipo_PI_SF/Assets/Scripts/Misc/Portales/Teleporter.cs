using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleporter : MonoBehaviour
{
    public GameObject portal;
    public GameObject player;
    public GameObject canvasTP;
    public GameManager gameManager = null;
    private ControlesJugador entrada;
    public PlayerController py;

    private static GameObject portalDestino;



    private void Awake()
    {
        entrada = new ControlesJugador();
        entrada.Enable();
        if (gameManager == null) gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.controles = entrada;
    }

    // Start is called before the first frame update
    void Start()
    {
        canvasTP.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
            /*if(entrada.controlesJugador.interactuar.ReadValue<float>() > 0f)
            {
                canvasTP.SetActive(true);
                py.canMove = false;
                portalDestino = portal;
            }*/
            py.canMove = false;
            canvasTP.SetActive(true);
            portalDestino = portal;
        }
    }

    public void Cerrar()
    {
        canvasTP.SetActive(false);
        py.canMove = true;
    }

    public void Teleport()
    {
        if (portal != portalDestino) return;
        
        player.transform.position = new Vector2(portalDestino.transform.position.x, portalDestino.transform.position.y);
        canvasTP.SetActive(false);
        py.canMove = true;
    }
}
