using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Confirmar : MonoBehaviour
{
    private PlayerController player = null;
    private GameManager gameManager = null;
    public GameObject canvasConfirmar;

    private void Start()
    {
        canvasConfirmar.SetActive(false);
        if (player == null) player = GameObject.FindObjectOfType<PlayerController>();
        if (gameManager == null) gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(GameManager.instance._dobleSaltoActivado == true)
            {
                player.canMove = false;
                canvasConfirmar.SetActive(true);
            }
            
        }
    }

    public void BotonConfirmar()
    {
        player.canMove = true;
        canvasConfirmar.SetActive(false);
        Destroy(gameObject);
    }
}
