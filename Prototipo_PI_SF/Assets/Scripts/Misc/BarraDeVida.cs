using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    
    public int numberOfHearts;
    public PlayerController player;

    public Image[] hearts;
    public Sprite contenedorVidaRelleno;
    public Sprite contenedorVida;


    void Update()
    {
        FuncionVida();
    }

    public void FuncionVida()
    {
        if (player.health > numberOfHearts)
        {
            player.health = numberOfHearts;
        }
        
        for (int i = 0; i < hearts.Length; i++)
        {
            if(i < player.health)
            {
                hearts[i].sprite = contenedorVidaRelleno;
            }

            else
            {
                hearts[i].sprite = contenedorVida;
            }

            if(i < numberOfHearts)
            {
                hearts[i].enabled = true;
            }

            else
            {
                hearts[i].enabled = false;
            }
        }
    }
}
